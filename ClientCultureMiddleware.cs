﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomMiddlewareExtension
{
    public class ClientCultureMiddleware
    {
        private readonly RequestDelegate _next;

        public ClientCultureMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, IClientConfiguration clientConfiguration)
        {
            if (httpContext.Request.Headers.TryGetValue("Culture", out StringValues Clutrename))
            {
             if(Clutrename.SingleOrDefault()== "EN")
                {
                    clientConfiguration.CultureName = "English";
                }
            }
            else
            {
                //Here you can throw exception to force client to send the header
            }

            clientConfiguration.InvokedDateTime = DateTime.UtcNow;

            //Move to next delegate/middleware in the pipleline
            await _next.Invoke(httpContext);
        }
    }
}
