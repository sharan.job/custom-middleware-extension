﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomMiddlewareExtension
{
  public  interface IClientConfiguration
    {
        string CultureName { get; set; }

        DateTime InvokedDateTime { get; set; }
    }
}
