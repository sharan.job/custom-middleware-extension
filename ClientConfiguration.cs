﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomMiddlewareExtension
{
    public class ClientConfiguration : IClientConfiguration
    {
        public string CultureName { get; set; }

        public DateTime InvokedDateTime { get; set; }
    }
}
