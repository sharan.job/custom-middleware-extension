﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomMiddlewareExtension.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationController : ControllerBase
    {
        private readonly IClientConfiguration clientConfiguration;

        public ConfigurationController(IClientConfiguration clientConfiguration)
        {
            this.clientConfiguration = clientConfiguration;
        }

        [HttpGet]
        public ActionResult<ClientConfiguration> GetCultureConfigurationController()
        {
            return new ClientConfiguration
            {
                CultureName = clientConfiguration.CultureName,
                InvokedDateTime = clientConfiguration.InvokedDateTime
            };
        }
    }
}